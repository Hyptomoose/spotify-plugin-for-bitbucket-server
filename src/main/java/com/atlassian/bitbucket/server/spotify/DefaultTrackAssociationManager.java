package com.atlassian.bitbucket.server.spotify;

import com.atlassian.bitbucket.pull.PullRequest;
import com.atlassian.fugue.Maybe;
import com.atlassian.fugue.Option;
import com.atlassian.sal.api.pluginsettings.PluginSettings;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Persists the associated between individual tracks and individual pull requests
 */
public class DefaultTrackAssociationManager implements TrackAssociationManager
{
    static final String SETTINGS_KEY = DefaultTrackAssociationManager.class.getName();
    private static final Logger log = LoggerFactory.getLogger(DefaultTrackAssociationManager.class);

    private final PluginSettings pluginSettings;

    public DefaultTrackAssociationManager(final PluginSettingsFactory pluginSettingsFactory)
    {
        pluginSettings = pluginSettingsFactory.createSettingsForKey(SETTINGS_KEY);
    }

    public void associate(final String trackId, final PullRequest pullRequest)
    {
        final String key = getKey(pullRequest);

        log.debug(String.format("Associating track %s with %s", trackId, key));
        pluginSettings.put(key, trackId);
    }

    public Maybe<String> getAssociation(final PullRequest pullRequest)
    {
        final String key = getKey(pullRequest);

        Object o = pluginSettings.get(key);
        if (o == null)
        {
            log.debug(String.format("%s was not associated with any tracks", key));
            return Option.none();
        }

        final String trackId = o.toString();

        log.debug(String.format("%s is associated with track %s", key, trackId));
        return Option.some(trackId);
    }

    private static String getKey(final PullRequest pullRequest)
    {
        return String.format("%s|%s", pullRequest.getToRef().getRepository().getId(), pullRequest.getId());
    }
}
