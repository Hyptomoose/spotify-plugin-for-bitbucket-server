package com.atlassian.bitbucket.server.spotify.feature.config;

import com.atlassian.sal.api.pluginsettings.PluginSettings;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;

/**
 * {@inheritDoc}
 */
public class DefaultConfigurationManager implements ConfigurationManager
{
    public static final String DEFAULT_TERRITORY = "US";
    private static final String TERRITORY_CODE = "TERRITORY_CODE";

    private final PluginSettings pluginSettings;

    public DefaultConfigurationManager(final PluginSettingsFactory pluginSettingsFactory)
    {
        this.pluginSettings = pluginSettingsFactory.createGlobalSettings();
    }

    @Override
    public void setTerritory(String territory)
    {
        pluginSettings.put(TERRITORY_CODE, territory);
    }

    @Override
    public String getTerritory()
    {
        Object value = pluginSettings.get(TERRITORY_CODE);
        if (value != null)
        {
            return value.toString();
        }
        return DEFAULT_TERRITORY;
    }
}
