package com.atlassian.bitbucket.server.spotify;

import com.atlassian.bitbucket.pull.PullRequest;
import com.atlassian.fugue.Maybe;

/**
 *
 */
public interface TrackAssociationManager
{
    public Maybe<String> getAssociation(final PullRequest pullRequest);
    public void associate(final String trackId, final PullRequest pullRequest);

}
