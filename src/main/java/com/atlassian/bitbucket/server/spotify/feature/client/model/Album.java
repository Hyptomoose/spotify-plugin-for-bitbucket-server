package com.atlassian.bitbucket.server.spotify.feature.client.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="album")
public class Album extends SpotifyModel
{
    @XmlElement(name="territories")
    private final String[] territories;

    public Album(final String href, final String name, final String[] territories)
    {
        super(href, name);
        this.territories = territories;
    }

    public String[] getTerritories()
    {
        return territories;
    }

    public boolean isValidTerritory(final String territory)
    {
        return contains(territories, territory);
    }

    private boolean contains(String[] array, String value)
    {
        for (String s : array)
        {
            if (s.equals(value))
            {
                return true;
            }
        }

        return false;
    }
}
