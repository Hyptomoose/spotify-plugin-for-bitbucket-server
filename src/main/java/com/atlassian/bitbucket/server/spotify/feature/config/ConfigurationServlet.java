package com.atlassian.bitbucket.server.spotify.feature.config;

import com.atlassian.plugin.webresource.WebResourceManager;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.UrlMode;
import com.atlassian.soy.renderer.SoyException;
import com.atlassian.soy.renderer.SoyTemplateRenderer;
import com.google.common.collect.Maps;
import org.apache.commons.lang3.StringUtils;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Renders a configuration UI for this add-on.
 */
public class ConfigurationServlet extends HttpServlet
{
    private final SoyTemplateRenderer soyTemplateRenderer;
    private final ApplicationProperties applicationProperties;
    private final WebResourceManager webResourceManager;
    private final ConfigurationManager configurationManager;

    public ConfigurationServlet(final SoyTemplateRenderer soyTemplateRenderer, final ApplicationProperties applicationProperties,
                                final WebResourceManager webResourceManager, final ConfigurationManager configurationManager)
    {
        this.soyTemplateRenderer = soyTemplateRenderer;
        this.applicationProperties = applicationProperties;
        this.webResourceManager = webResourceManager;
        this.configurationManager = configurationManager;
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
       renderView(resp, false);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        String territory = req.getParameter("territory");
        if (!StringUtils.isBlank(territory))
        {
            configurationManager.setTerritory(territory);
        }
        renderView(resp, true);
    }

    private void renderView(HttpServletResponse response, boolean showSuccessMessage) throws ServletException, IOException
    {
        webResourceManager.requireResourcesForContext("com.atlassian.bitbucket.server.spotify.feature.config");

        HashMap<String,Object> data = Maps.newHashMap();
        data.put("contextPath", applicationProperties.getBaseUrl(UrlMode.RELATIVE));
        data.put("territory", configurationManager.getTerritory());
        data.put("showSuccess", showSuccessMessage);

        render(response, "com.atlassian.bitbucket.server.spotify.feature.config.configLayout", data);
    }

    private void render(HttpServletResponse resp, String templateName, Map<String, Object> data) throws IOException, ServletException
    {
        resp.setContentType("text/html;charset=UTF-8");
        try {
            soyTemplateRenderer.render(resp.getWriter(),
                    "com.atlassian.bitbucket.server.bitbucket-spotify-plugin:feature-config-templates",
                    templateName,
                    data);
        } catch (SoyException e) {
            Throwable cause = e.getCause();
            if (cause instanceof IOException) {
                throw (IOException) cause;
            }
            throw new ServletException(e);
        }
    }


}
