package com.atlassian.bitbucket.server.spotify.feature.client.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * Base model for all the JSON entities returned from the Spotify Web API.
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class SpotifyModel
{
    @XmlElement(name="href")
    private final String href;

    @XmlElement(name="name")
    private final String name;

    public SpotifyModel(final String href, final String name)
    {
        this.href = href;
        this.name = name;
    }

    public String getHref()
    {
        return href;
    }

    public String getName()
    {
        return name;
    }
}
