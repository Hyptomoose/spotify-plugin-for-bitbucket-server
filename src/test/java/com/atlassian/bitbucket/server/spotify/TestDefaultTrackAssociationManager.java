package com.atlassian.bitbucket.server.spotify;

import com.atlassian.bitbucket.pull.PullRequest;
import com.atlassian.bitbucket.pull.PullRequestRef;
import com.atlassian.bitbucket.repository.Repository;
import com.atlassian.fugue.Maybe;
import com.atlassian.sal.api.pluginsettings.PluginSettings;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.*;
import static org.junit.Assert.*;

/**
 * Test harness for {@link DefaultTrackAssociationManager}.
 */
public class TestDefaultTrackAssociationManager
{
    private static final String TEST_TRACK_ID = "spotify:track:5421318643512hd45";
    private static final Long TEST_PULL_REQUEST_ID = 2L;
    private static final Integer TEST_REPO_ID = 1234;

    @Mock
    private PluginSettingsFactory pluginSettingsFactory;
    @Mock
    private PluginSettings pluginSettings;

    private TrackAssociationManager trackAssociationManager;

    @Before
    public void before()
    {
        MockitoAnnotations.initMocks(this);
        when(pluginSettingsFactory.createSettingsForKey(DefaultTrackAssociationManager.SETTINGS_KEY)).thenReturn(pluginSettings);

        trackAssociationManager = new DefaultTrackAssociationManager(pluginSettingsFactory);
    }

    @Test
    public void testPutAssociation()
    {
        PullRequest pr = getMockPullRequest();

        trackAssociationManager.associate(TEST_TRACK_ID, pr);

        final String expectedKey = TEST_REPO_ID + "|" + TEST_PULL_REQUEST_ID;
        verify(pluginSettings).put(expectedKey, TEST_TRACK_ID);
    }

    @Test
    public void testGetAssociation()
    {
        final String expectedKey = TEST_REPO_ID + "|" + TEST_PULL_REQUEST_ID;

        PullRequest pr = getMockPullRequest();
        when(pluginSettings.get(expectedKey)).thenReturn(TEST_TRACK_ID);


        Maybe<String> association = trackAssociationManager.getAssociation(pr);
        assertTrue(association.isDefined());
        assertEquals(TEST_TRACK_ID, association.get());
    }

    @Test
    public void testGetAssociation_Miss()
    {
        PullRequest pr = getMockPullRequest();

        Maybe<String> association = trackAssociationManager.getAssociation(pr);

        assertTrue(association.isEmpty());
        assertNull(association.getOrNull());
    }

    private PullRequest getMockPullRequest()
    {
        PullRequest pr = mock(PullRequest.class);
        when(pr.getId()).thenReturn(TEST_PULL_REQUEST_ID);
        PullRequestRef prr = mock(PullRequestRef.class);
        when(pr.getToRef()).thenReturn(prr);
        Repository repo = mock(Repository.class);
        when(prr.getRepository()).thenReturn(repo);
        when(repo.getId()).thenReturn(TEST_REPO_ID);

        return pr;
    }

}
