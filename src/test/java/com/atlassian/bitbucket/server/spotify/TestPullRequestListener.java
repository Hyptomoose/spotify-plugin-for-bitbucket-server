package com.atlassian.bitbucket.server.spotify;

import com.atlassian.bitbucket.event.pull.PullRequestOpenedEvent;
import com.atlassian.bitbucket.pull.PullRequest;
import com.atlassian.bitbucket.pull.PullRequestRef;
import com.atlassian.bitbucket.pull.PullRequestService;
import com.atlassian.bitbucket.pull.PullRequestUpdateRequest;
import com.atlassian.bitbucket.repository.Repository;
import org.hamcrest.Matcher;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentMatcher;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.*;

/**
 * Test harness for {@link PullRequestListener}
 */
public class TestPullRequestListener
{
    final String embeddedTrackId = "spotify:track:3AhXZa8sUQht0UEdBJgpGc";
    final String embeddedTrackMarkdown = "[" + embeddedTrackId + "](https://embed.spotify.com)";
    final String embeddedTrackPreamble = "This is my _markdown text_";
    final String embeddedTrackDescription = embeddedTrackPreamble + embeddedTrackMarkdown;

    final String noEmbeddedTrackDescription = "I have nothing to say to you. Goodbye";

    @Mock
    private PullRequestService pullRequestService;
    @Mock
    private TrackAssociationManager trackAssociationManager;

    private PullRequestListener pullRequestListener;

    @Before
    public void before()
    {
        MockitoAnnotations.initMocks(this);

        pullRequestListener = new PullRequestListener(pullRequestService, trackAssociationManager);
    }

    @Test
    public void testMatchDescription()
    {
        PullRequest pr = mock(PullRequest.class);
        when(pr.getDescription()).thenReturn(embeddedTrackDescription);
        when(pr.getVersion()).thenReturn(1);
        when(pr.getTitle()).thenReturn("This is my pull request");

        PullRequestRef toRef = mock(PullRequestRef.class);
        Repository repo = mock(Repository.class);
        when(repo.getId()).thenReturn(123);
        when(toRef.getRepository()).thenReturn(repo);
        when(pr.getToRef()).thenReturn(toRef);

        when(pr.getId()).thenReturn(1L);

        PullRequestOpenedEvent e = new PullRequestOpenedEvent(this, pr);

        PullRequest updatedPr = mock(PullRequest.class);

        Matcher<PullRequestUpdateRequest> m = new ArgumentMatcher<PullRequestUpdateRequest>()
        {
            @Override
            public boolean matches(Object o)
            {
                if (o == null) return false;
                if (!(o instanceof PullRequestUpdateRequest)) return false;

                PullRequestUpdateRequest m = (PullRequestUpdateRequest)o;

                return embeddedTrackPreamble.equals(m.getDescription());
            }
        };


        when(pullRequestService.update(Matchers.argThat(m))).thenReturn(updatedPr);


        pullRequestListener.onPullRequestOpened(e);

        verify(trackAssociationManager).associate(embeddedTrackId, updatedPr);
    }

    @Test
    public void testNoMatchDescription()
    {
        PullRequest pr = mock(PullRequest.class);
        when(pr.getDescription()).thenReturn(noEmbeddedTrackDescription);
        when(pr.getVersion()).thenReturn(1);
        when(pr.getTitle()).thenReturn("This is my pull request");

        PullRequestRef toRef = mock(PullRequestRef.class);
        Repository repo = mock(Repository.class);
        when(repo.getId()).thenReturn(123);
        when(toRef.getRepository()).thenReturn(repo);
        when(pr.getToRef()).thenReturn(toRef);

        when(pr.getId()).thenReturn(1L);

        PullRequestOpenedEvent e = new PullRequestOpenedEvent(this, pr);

        pullRequestListener.onPullRequestOpened(e);

        verifyZeroInteractions(pullRequestService, trackAssociationManager);
    }

}
