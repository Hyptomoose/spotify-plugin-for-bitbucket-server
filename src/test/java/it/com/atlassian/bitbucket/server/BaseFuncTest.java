package it.com.atlassian.bitbucket.server;

import com.atlassian.bitbucket.test.DefaultFuncTestData;
import com.atlassian.bitbucket.test.runners.FuncTestSplitterRunner;
import com.atlassian.pageobjects.TestedProductFactory;
import com.atlassian.webdriver.bitbucket.BitbucketTestedProduct;
import com.atlassian.webdriver.bitbucket.page.BitbucketLoginPage;
import com.atlassian.webdriver.bitbucket.page.BitbucketPage;
import com.atlassian.webdriver.testing.rule.WebDriverScreenshotRule;
import com.atlassian.webdriver.testing.rule.WindowSizeRule;
import it.com.atlassian.bitbucket.server.rules.SessionCleanupRule;
import org.junit.AfterClass;
import org.junit.Rule;
import org.junit.runner.RunWith;

@RunWith(FuncTestSplitterRunner.class)
public abstract class BaseFuncTest {

    protected final BitbucketTestedProduct BITBUCKET = TestedProductFactory.create(BitbucketTestedProduct.class);

    @Rule
    public SessionCleanupRule cleanupRule = new SessionCleanupRule();

    @Rule
    public WebDriverScreenshotRule screenshotRule = new WebDriverScreenshotRule();

    @Rule
    public final WindowSizeRule windowSizeRule = new WindowSizeRule();

    @AfterClass
    public static void clearBrowserCookies() {
        TestedProductFactory.create(BitbucketTestedProduct.class).getTester().getDriver().manage().deleteAllCookies();
    }

    protected <P extends BitbucketPage> P loginAsAdmin(Class<P> page, Object... args) {
        return loginAs(DefaultFuncTestData.getAdminUser(), DefaultFuncTestData.getAdminPassword(), page, args);
    }

    protected <P extends BitbucketPage> P loginAsRegularUser(Class<P> page, Object... args) {
        return loginAs(DefaultFuncTestData.getRegularUser(), DefaultFuncTestData.getRegularUserPassword(), page, args);
    }

    protected <P extends BitbucketPage> P anonymous(Class<P> page, Object... args) {
        clearBrowserCookies();
        return BITBUCKET.visit(page, args);
    }

    protected <P extends BitbucketPage> P loginAs(String username, String password, Class<P> page, Object... args) {
        clearBrowserCookies();
        return BITBUCKET.visit(BitbucketLoginPage.class).login(username, password, page, args);
    }

}
