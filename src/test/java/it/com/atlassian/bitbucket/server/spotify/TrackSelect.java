package it.com.atlassian.bitbucket.server.spotify;

import javax.annotation.Nonnull;

import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.webdriver.bitbucket.element.IdSelect2;

/**
 * TODO: Document this class / interface here
 *
 * @since v6.1
 */
public class TrackSelect extends IdSelect2<TrackSelect, TrackOption>
{

    public TrackSelect(PageElement container) {
        super(container, TrackOption.class);
    }

    @Nonnull
    @Override
    protected TrackSelect self()
    {
        return this;
    }
}
